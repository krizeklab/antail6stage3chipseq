#!/usr/bin/env perl

# motif_distances.pl
# Version: 2021-04-18
#
# Copyright (C) 2021 by Kevin Higgins <khigginsk@gmail.com>, Metagenetics, LLC.
#
# All rights reserved.

# Purpose: A tool for calculating the distances between motif positions on chromosomes.
#
# Usage:
#
# perl motif_distances.pl --positions1_file fileA.gff --positions2_file fileB.gff        (The first file is fileA.gff, the second file is fileB.gff)
# perl motif_distances.pl --positions1_file fileB.gff --positions2_file fileA.gff        (The first file is fileB.gff, the second file is fileA.gff)
# perl motif_distances.pl --positions1_file fileA.gff --positions2_file fileA.gff        (The same file may be used twice)
# perl motif_distances.pl --positions1_file ANTMEME2ANTAIL6peaks.gff --positions2_file AIL6MEME2ANTAIL6peaks.gff  (Examples of actual file names)
# perl motif_distances.pl --positions1_file fileA.gff --positions2_file fileB.gff --neighbors 4                   (The number of neighbors)
# perl motif_distances.pl --chrom_col 1                                                  (The file column number holding the chromosome ids)
# perl motif_distances.pl --start_col 4                                                  (The file column number holding the start positions)
# perl motif_distances.pl --stop_col 5                                                   (The file column number holding the stop positions)
#
# Inputs: Two tab separated files containing the chromosome ids and positions of the two motifs.
#
# Example file.gff
#
# ##gff	##gff-version	3	0	0				
# 1	fimo	nucleotide_motif	76158	76176	48.1	-	.	Name=2_1:75810-76496-;Alias=;ID=2-1-1:75810-76496;pvalue=1.55e-05;qvalue=0.164;sequence=ACCGGTCTCGAAAACCGGG;
# 1	fimo	nucleotide_motif	180991	181009	47.5	-	.	Name=2_1:180674-181355-;Alias=;ID=2-1-1:180674-181355;pvalue=1.77e-05;qvalue=0.176;sequence=TGCTTCTTTGAAATACGTG;
# 1	fimo	nucleotide_motif	227735	227753	45.5	+	.	Name=2_1:227310-228500+;Alias=;ID=2-1-1:227310-228500;pvalue=2.84e-05;qvalue=0.207;sequence=CATAGTCTCGAACATTGTG;
#
# Outputs: The distances between the motif pairings

use strict;
use warnings FATAL => 'all';                                                    # Warnings are fatal
use diagnostics;

use Getopt::Long;

# Command line defaults for this Perl script.
# See http://www.perlmonks.org/?node_id=455180 for original version.
my $positions1_file = 'fileA.gff';                                    # File holding the positions of the first motif. Default value (fileA.gff).
my $positions2_file = 'fileB.gff';                                    # File holding the positions of the second motif. Default value (fileB.gff).
my $neighbors = 4;                                                    # The number of neighbors. Default value (4).
my $chrom_col = 1;                                                    # The file column number holding the chromosome ids. Default value (1).
my $start_col = 4;                                                    # The file column number holding the start positions. Default value (4).
my $stop_col = 5;                                                     # The file column number holding the stop positions. Default value (5).

# "The call to GetOptions() parses the command line arguments that are present in @ARGV", http://perldoc.perl.org/Getopt/Long.html

GetOptions(
  'positions1_file=s' => \$positions1_file,
  'positions2_file=s' => \$positions2_file,
  'neighbors=i' => \$neighbors,
  'chrom_col=i' => \$chrom_col,
  'start_col=i' => \$start_col,
  'stop_col=i' => \$stop_col,
) or die "Incorrect usage!\n";

# My perl arrays are 0-based so lets adjust the column numbers
$chrom_col = $chrom_col - 1;
$start_col = $start_col - 1;
$stop_col = $stop_col - 1;

# Let's read the first motif file and get the chromosome tags, chromosome ids and positions

my @tags1;
my @first_AoA;                                                                  # Array of arrays is accessed with subscripts, like $AoA[3][2]
                                                                                # https://perldoc.perl.org/perllol
use Scalar::Util qw(looks_like_number);

open (my $infile, '<', "$positions1_file") or die "Can't open < $positions1_file: $!";
while (my $line = <$infile>)
{
  next if ($line =~ /^\s*(#.*)?$/);                                             # Regular expression to ignore lines starting with #,
                                                                                # and empty lines, https://www.perlmonks.org/?node_id=259230
                                                                                # Also, https://docstore.mik.ua/orelly/perl3/cookbook/ch06_11.htm

  chomp($line);                                                                 # Strip record separator

  # If gff file
  # 0    1                2     3     4
  # 1 fimo nucleotide_motif 76158 76176  (rest of line truncated)

  # If bed file
  #    0        1       2
  # Chr1    76157   76175

  my @columns = split('\t', $line);                                             # File has tabs between columns

  # If either column $start_col does not look like a number, or column $stop_col does not look like a number, then ignore line
  if ( !(looks_like_number($columns[$start_col]) && looks_like_number($columns[$stop_col])) )
  {
    next;
  }
  # If here then likely to be a row of data

  push(@tags1, $columns[$chrom_col]);
  push(@first_AoA, [ @columns ]);                                               # https://perldoc.perl.org/perllol
}
close($infile) || warn "close failed: $!";

# Let's reduce the tags array so that it only holds the unique chromosome ids
# Unique values in an array in Perl
# https://perlmaven.com/unique-values-in-an-array-in-perl

use List::MoreUtils qw(uniq);
@tags1 = uniq(@tags1);

# Let's read the second motif file and get the chromosome tags, chromosome ids and positions

my @tags2;
my @second_AoA;                                                                 # Array of arrays is accessed with subscripts, like $AoA[3][2]
                                                                                # https://perldoc.perl.org/perllol

open ($infile, '<', "$positions2_file") or die "Can't open < $positions2_file: $!";
while (my $line = <$infile>)
{
  next if ($line =~ /^\s*(#.*)?$/);                                             # Regular expression to ignore lines starting with #,
                                                                                # and empty lines, https://www.perlmonks.org/?node_id=259230
                                                                                # Also, https://docstore.mik.ua/orelly/perl3/cookbook/ch06_11.htm

  chomp($line);                                                                 # Strip record separator

  # If gff file
  # 0    1                2     3     4
  # 1 fimo nucleotide_motif 76158 76176  (rest of line truncated)

  # If bed file
  #    0        1       2
  # Chr1    76157   76175

  my @columns = split('\t', $line);                                             # File has tabs between columns

  # If either column $start_col does not look like a number, or column $stop_col does not look like a number, then ignore line
  if ( !(looks_like_number($columns[$start_col]) && looks_like_number($columns[$stop_col])) )
  {
    next;
  }
  # If here then likely to be a row of data

  push(@tags2, $columns[$chrom_col]);
  push(@second_AoA, [ @columns ]);                                              # https://perldoc.perl.org/perllol
}
close($infile) || warn "close failed: $!";

# Let's reduce the tags array so that it only holds the unique chromosome ids
# Unique values in an array in Perl
# https://perlmaven.com/unique-values-in-an-array-in-perl

use List::MoreUtils qw(uniq);
@tags2 = uniq(@tags2);

# Let's print out the positions chromosome by chromosome

use sort 'stable';                                                              # Guarantee stability, https://perldoc.perl.org/sort

foreach my $tag (@tags1)
{
  # If gff file
  # 0    1                2     3     4
  # 1 fimo nucleotide_motif 76158 76176  (rest of line truncated)
  #
  # If bed file
  #    0        1       2
  # Chr1    76157   76175
  #
  # Accessing array of arrays, https://perldoc.perl.org/perllol
  # For loop, https://stackoverflow.com/a/974819
  # Last array element, http://www.perlmonks.org/?node_id=44637

  my @table_AoA = ();

  for my $i (0 .. $#first_AoA)
  {
    if (uc($first_AoA[$i][$chrom_col]) eq uc($tag))                             # Case insensitive match, https://stackoverflow.com/a/3399155
    {
      for my $j (0 .. $#second_AoA)
      {
        if (uc($second_AoA[$j][$chrom_col]) eq uc($tag))
        {
          my $p1 = ($first_AoA[$i][$start_col] + $first_AoA[$i][$stop_col])/2.0;
          my $p2 = ($second_AoA[$j][$start_col] + $second_AoA[$j][$stop_col])/2.0;
          my @tmp = (abs($p1-$p2), $p1, $p2, $tag);
          push(@table_AoA, [ @tmp ]);                                           # https://perldoc.perl.org/perllol
        }
      }
    }
  }

  @table_AoA = sort { $a->[0] <=> $b->[0] } @table_AoA;                         # Stable sort based on distance (i.e., column 0)
                                                                                # https://stackoverflow.com/a/10318668

  # Get rid of path and suffix so that the input file names can be used to create the output file names
  # https://perldoc.perl.org/File::Basename
  use File::Basename;
  my($p1f, $p1d, $p1s) = fileparse($positions1_file);
  my($p2f, $p2d, $p2s) = fileparse($positions2_file);
  substr($p1f,rindex $p1f,'.') = '';                                            # Remove .gff suffix, http://www.perlmonks.org/?node_id=729477
  substr($p2f,rindex $p2f,'.') = '';

  my $chrom_label = $tag;
  if (looks_like_number($chrom_label))
  {
    $chrom_label = 'chr' . $tag;
  }

  # Create the output file names
  my $filename = 'distances_from_' . $p1f . '_to_' . $p2f . '_' . "$chrom_label" . '.txt';
  open (my $outfile, '>', "$filename") or die "Can't open > $filename: $!";
  printf $outfile ("%10s %12s %12s %12s\n", "Distance", "Position1", "Position2" , "Chromosome");

  for my $i (0 .. $#table_AoA)
  {
    printf $outfile ("%10d %12d %12d %12s\n", $table_AoA[$i][0], $table_AoA[$i][1], $table_AoA[$i][2], $table_AoA[$i][3]);
  }
  close($outfile) || warn "close failed: $!";

  # Save nearest-neighbor, and next-nearest-neighbor, and next-next-next-neighbor

  @table_AoA = sort { $a->[1] <=> $b->[1] } @table_AoA;                         # Stable sort based on Position1 (i.e., column 1)
                                                                                # https://stackoverflow.com/a/10318668

  my @positions1;
  for my $i (0 .. $#table_AoA)
  {
    # $table_AoA
    # Distance      Position1    Position2   Chromosome
    # 2                 76166        76164            1

    push(@positions1, $table_AoA[$i][1]);
  }

  # Let's reduce the positions1 array so that it only holds the unique positions
  # Unique values in an array in Perl
  # https://perlmaven.com/unique-values-in-an-array-in-perl
  @positions1 = uniq(@positions1);                                              # use List::MoreUtils qw(uniq), declared above

  my $filenameNN = 'nearest-neighbors_from_' . $p1f . '_to_' . $p2f . '_' . "$chrom_label" . '.txt';
  open (my $outfileNN, '>', "$filenameNN") or die "Can't open > $filenameNN $!";

  # printf $outfileNN ("%10s %12s %12s", "DistanceAB", "DistanceAC", "DistanceAD");
  my $label = 'AB';
  for my $i (0 .. $neighbors-1)
  {
    my $format = ' %12s';
    if ($label eq 'AB')
    {
      $format = '%10s';
    }
    printf $outfileNN ($format, 'Distance' . $label);
    ++$label;
  }

  # printf $outfileNN ("%12s %12s %12s %12s", "PositionA", "PositionB", "PositionC", "PositionD");
  $label = 'A';
  for my $i (0 .. $neighbors)
  {
    my $format = ' %12s';
    if ($label eq 'A')
    {
      $format = '%12s';
    }
    printf $outfileNN ($format, 'Position' . $label);
    ++$label;
  }

  printf $outfileNN ("%12s\n", "Chromosome");

  foreach my $p (@positions1)
  {
    # $table_AoA
    # Distance      Position1    Position2   Chromosome
    # 2                 76166        76164            1

    my $count = 0;
    my @distances = ();                                                         # Enlarge using assignment, https://stackoverflow.com/a/31829252
    my @p_x = ();
    for my $i (0 .. $#table_AoA)
    {
      if ($table_AoA[$i][1] == $p)
      {
        if ($count < $neighbors-1)
        {
          $distances[$count] = $table_AoA[$i][0];
          $p_x[$count] = $table_AoA[$i][2];
          $count++;
        }
        elsif ($count == $neighbors-1)
        {
          $distances[$count] = $table_AoA[$i][0];
          $p_x[$count] = $table_AoA[$i][2];

          # printf $outfileNN ("%10s %12s %12s", $distances[0], $distances[1], $distances[2]);
          # printf $outfileNN ("%12s %12s %12s %12s", $p, $p_x[0], $p_x[1], $p_x[2]);
          # printf $outfileNN ("%12s\n", $table_AoA[$i][3]);
          for my $j (0 .. $neighbors-1)
          {
            my $format = ' %12s';
            if ($j == 0)
            {
              $format = '%10s';
            }
            printf $outfileNN ($format, $distances[$j]);
          }
          printf $outfileNN ('%12s', $p);
          for my $j (0 .. $neighbors-1)
          {
            my $format = ' %12s';
            printf $outfileNN ($format, $p_x[$j]);
          }
          printf $outfileNN ("%12s\n", $table_AoA[$i][3]);

          $count++;
        }
        else
        {
          next;
        }
      }
    }
  }

  close($outfileNN) || warn "close failed: $!";
}
