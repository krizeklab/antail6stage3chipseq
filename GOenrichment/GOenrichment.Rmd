---
title: "Identification of enriched GO terms"
author: "Beth Krizek"
date: "4/19/2020"
output: html_document
---

## Introduction

Here we will obtain enriched GO terms for the genes associated with ChIP-Seq peaks for AIL6 and ANT in stage 3 flowers and ANT in stage 6/7 flowers using a threshold value of 2.5. 

* Here we ask the following questions:
* Are GO terms associated with floral organ development enriched in the set of closest genes?
* Are GO terms associated with polarity genes enriched in the set of closest genes?
* Are GO terms associated with hormone signaling enriched in the set of closest genes?

## Methods

## Stage 3 ChIP-Seq peaks (threshold 2.5)
### Generate AIL6 file with unique gene model identifiers for GO term enrichiment analysis using AmiGO
```{r} 
myAIL6datat2h <- read.table ('../FindClosestGeneVA/results/AIL6allannotbotht2h.csv', header=TRUE, sep=",")
nrow(myAIL6datat2h)
AIL6closestgeneIDst2h <- as.vector(myAIL6datat2h[,7])
AIL6uniqueclosestgeneIDst2h <- unique(AIL6closestgeneIDst2h)
length(AIL6uniqueclosestgeneIDst2h)
write.table(AIL6uniqueclosestgeneIDst2h, './results/AIL6uniqueclosestgeneIDs2th.txt', sep = "\t", col.names = F, row.names = F, quote = F)
```

### Generate ANT file with unique gene model identifiers for GO term enrichiment analysis using AmiGO
```{r} 
myANTdatat2h <- read.table ('../FindClosestGeneVA/results/ANTallannotbotht2h.csv', header=TRUE, sep=",")
ANTclosestgeneIDst2h <- as.vector(myANTdatat2h[,7])
ANTuniqueclosestgeneIDst2h <- unique(ANTclosestgeneIDst2h)
length(ANTuniqueclosestgeneIDst2h)
write.table(ANTuniqueclosestgeneIDst2h, './results/ANTuniqueclosestgeneIDst2h.txt', sep = "\t", col.names = F, row.names = F, quote = F)
```

## Stage 6/7 ANT ChIP-Seq peaks (threshold 2.5)
### Generate ANT file with unique gene model identifiers for GO term enrichiment analysis using AmiGO
```{r} 
myst6ANTdatat2h <- read.table ('../FindClosestGeneVA/results/st6ANTallannotbotht2h.csv', header=TRUE, sep=",")
st6ANTclosestgeneIDst2h <- as.vector(myst6ANTdatat2h[,7])
st6ANTuniqueclosestgeneIDst2h <- unique(st6ANTclosestgeneIDst2h)
length(st6ANTuniqueclosestgeneIDst2h)
write.table(st6ANTuniqueclosestgeneIDst2h, './results/st6ANTuniqueclosestgeneIDst2h.txt', sep = "\t", col.names = F, row.names = F, quote = F)
```

## Session info

```{r}
sessionInfo()
```

## Results
* GO output file "st3AIL6t2hGO.xlsx"
* GO output file "st3ANTt2hGO.xlsx"
* GO output file "st6ANTt2hGO.xlsx"

* stage 3 AIL6 GO terms

The following enriched GO biological process terms are enriched in genes associated with AIL6 ChIP-Seq peaks in stage 3 flowers: formation of plant organ boundary, radial pattern formation, polarity specification of adaxial/abaxial axis, floral meristem determinacy, meristem initiation, cell fate specification, maintenance of meristem identity, floral organ morphogenesis, plant organ formation, plant ovule development, cotyledon development, shoot system morphogenesis, regulation of flower development, auxin-activated signaling pathway, regulation of hormone levels, regulation of transcription, DNA-templated. The following enriched GO molecular function terms are enriched in genes associated with AIL6 ChIP-Seq peaks in stage 3 flowers: DNA-binding transcription factor activity, transcription regulatory region DNA binding, protein dimerization activity, and sequence-specific DNA binding. The following enriched GO cellular component terms are enriched in genes associated with AIL6 ChIP-seq peaks in stage 3 flowers: glyoxysome and nucleus.

* stage 3 ANT GO terms

The following enriched GO biological process terms are enriched in genes associated with ANT ChIP-Seq peaks in stage 3 flowers: polarity specification of adaxial/abaxial axis, floral meristem determinacy, radial pattern formation, floral organ formation, stomatal complex morphogenesis, cell fate specification, meristem initiation, embryonic meristem development, maintenance of meristem identity, formation of plant organ boundary, guard cell differentiation, meristem initiation, maintenance of meristem identity, plant ovule development,cotyledon development, cytokinin-activated signaling pathway, auxin polar transport, anther development, auxin-activated signaling pathway, regulation of flower development, response to gibberellin, leaf morphogenesis, regulation of growth, and positive regulation of transcription, DNA-templated. The following enriched GO molecular function terms are enriched in genes associated with ANT ChIP-Seq peaks in stage 3 flowers: DNA-binding transcription factor activity, transcription regulatory region DNA binding, protein homodimerization activity, and sequence-specific DNA binding. The following enriched GO cellular component term is enriched in genes associated with ANT ChIP-seq peaks in stage 3 flowers: nucleus. 

* stage 6/7 ANT GO terms

The following enriched GO biological process terms are enriched in genes associated with ANT ChIP-Seq peaks in stage 6/7 flowers: polarity specification of adaxial/abaxial axis, guard cell differentiation, radial pattern formation, floral organ formation, meristem initiation, fatty acid beta-oxidation, maintenance of meristem identity, plant ovule development, stamen development, leaf morphogenesis, response to gibberellin, auxin-activated signaling pathway, regulation of transcription, and regulation of growth. The following enriched GO molecular function terms are enriched in genes associated with ANT ChIP-Seq peaks in stage 6/7 flowers: protein dimerization activity, DNA-binding transcription factor activity, transcription regulatory region DNA binding, sequence-specific DNA binding. The following enriched GO cellular component terms are enriched in genes associated with ANT ChIP-seq peaks in stage 6/7 flowers: peroxisomal membrane and nucleus.