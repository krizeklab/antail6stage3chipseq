Query_ID	Target_ID	Optimal_offset	p-value	E-value	q-value	Overlap	Query_consensus	Target_consensus	Orientation
1	1	0	0	0	0	21	CCTCTCTCTCTCTCTTTTTTT	CCTCTCTCTCTCTCTTTTTTT	+
1	5	-1	0.000148247	0.0025202	0.0025202	15	CCTCTCTCTCTCTCTTTTTTT	TTTTGTTTTTTTTTT	-
1	6	-6	0.00265735	0.045175	0.0301166	7	CCTCTCTCTCTCTCTTTTTTT	TCTTTTT	-
1	3	-6	0.00425843	0.0723934	0.0361967	7	CCTCTCTCTCTCTCTTTTTTT	TCTCTCT	-
2	2	0	0	0	0	19	AACTGCCTCGAGATTCGTG	AACTGCCTCGAGATTCGTG	+
2	11	-5	0.00163684	0.0278263	0.0185509	8	AACTGCCTCGAGATTCGTG	CCTCGAGA	+
2	1	1	0.00884913	0.150435	0.0752176	19	AACTGCCTCGAGATTCGTG	CCTCTCTCTCTCTCTTTTTTT	+
3	3	0	1.4017e-08	2.38289e-07	4.76579e-07	7	AGAGAGA	AGAGAGA	+
3	6	0	0.00579979	0.0985965	0.0985965	7	AGAGAGA	AAAAAGA	+
4	4	0	3.31874e-09	5.64185e-08	1.12837e-07	6	CACATG	CACATG	+
4	8	0	0.00123404	0.0209788	0.0209788	6	CACATG	CACGTGGCATATACA	-
4	16	1	0.00425792	0.0723847	0.0482565	6	CACATG	CCACTTTC	+
5	5	0	3.85729e-37	6.55739e-36	1.31148e-35	15	AAAAAAAAAACAAAA	AAAAAAAAAACAAAA	+
5	1	5	0.000226402	0.00384883	0.00384883	15	AAAAAAAAAACAAAA	AAAAAAAGAGAGAGAGAGAGG	-
5	10	-9	0.000750247	0.0127542	0.0085028	6	AAAAAAAAAACAAAA	ACAAAA	+
6	6	0	4.36516e-09	7.42077e-08	1.48415e-07	7	AAAAAGA	AAAAAGA	+
7	7	0	5.70609e-10	9.70036e-09	1.94007e-08	7	ATAAATA	ATAAATA	+
8	8	0	5.73771e-35	9.75411e-34	1.95082e-33	15	TGTATATGCCACGTG	TGTATATGCCACGTG	+
8	4	-9	6.87359e-05	0.00116851	0.00116851	6	TGTATATGCCACGTG	CATGTG	-
9	9	0	9.08338e-07	1.54417e-05	3.08835e-05	5	CCCAA	CCCAA	+
10	10	0	9.18681e-09	1.56176e-07	3.12352e-07	6	ACAAAA	ACAAAA	+
10	5	9	0.00533206	0.0906451	0.0906451	6	ACAAAA	AAAAAAAAAACAAAA	+
11	11	0	3.70014e-11	6.29025e-10	1.25805e-09	8	CCTCGAGA	CCTCGAGA	+
11	14	-2	0.00389875	0.0662788	0.0609257	6	CCTCGAGA	TCGAGGC	-
11	2	5	0.0053758	0.0913886	0.0609257	8	CCTCGAGA	AACTGCCTCGAGATTCGTG	+
12	12	0	1.12837e-07	1.91823e-06	3.83646e-06	6	GTGACA	GTGACA	+
13	13	0	1.48006e-09	2.5161e-08	5.0322e-08	8	CCGTTGGA	CCGTTGGA	+
14	14	0	1.19145e-08	2.02546e-07	4.05092e-07	7	GCCTCGA	GCCTCGA	+
14	11	-1	0.00883499	0.150195	0.0989321	6	GCCTCGA	TCTCGAGG	-
15	15	0	2.04852e-09	3.48249e-08	6.96498e-08	8	ACTACTAC	ACTACTAC	+
16	16	0	1.48006e-09	2.5161e-08	5.0322e-08	8	CCACTTTC	CCACTTTC	+
17	17	0	5.22373e-09	8.88035e-08	1.77607e-07	8	AGAAGAAG	AGAAGAAG	+

# Tomtom (Motif Comparison Tool): Version 5.0.5 compiled on Aug 23 2019 at 17:35:21
# The format of this file is described at http://meme-suite.org//doc/tomtom-output-format.html.
# tomtom -verbosity 1 -text -thresh 0.1 memechip_out/combined.meme memechip_out/combined.meme
